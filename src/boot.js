FBInstant.initializeAsync().then(function() {

    var config = {
        type: Phaser.CANVAS,
        scale: {
            mode: Phaser.Scale.FIT,
            autoCenter: Phaser.Scale.CENTER_BOTH,
            width: 1920,
            height: 1080,
            parent: 'gameDiv'
        },
        backgroundColor: 0xffeedd,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: { y: 200 }
            }
        },
        scene: [Loader, LevelSelect, GameScreen, GameOver, PauseMenu, LevelWon]
    };

    var game = new Phaser.Game(config);
});

var Loader = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

    function Loader ()
    {
        Phaser.Scene.call(this, { key: 'Loader' });
    },

    preload: function ()
    {
        this.facebook.once('startgame', this.startGame, this);
        this.facebook.showLoadProgress(this);

        this.load.image('button', 'media/button.png');
        this.load.image('cannon1', 'media/cannon.png');
        this.load.image('cannon2', 'media/cannon2.png');
        this.load.image('enemy1', 'media/enemy.png');
        this.load.image('dialog', 'media/dialog.png');
        this.load.image('path', 'media/path.png');
        this.load.image('stone', 'media/stone.jpg');
        this.load.image('kitten1', 'media/kitten1.png');
        this.load.image('projectile', 'media/Projectile.png');

    },

    create: function()
    {
        this.loadCannonJSON();
    },

    loadCannonJSON: function(){
        var request = new XMLHttpRequest();
        var game = this.game;

        request.open('GET', 'src/JSON/cannon.json', true);

        request.onload = function(){ 
            if (request.status >= 200 && request.status < 400) {
            // Success!
            game.cannon_config = JSON.parse(request.responseText);
            } else {
                // We reached our target server, but it returned an error

            }
        };

        request.send();
    },

    startGame: function()
    {
        this.scene.start('LevelSelect');
    }
})

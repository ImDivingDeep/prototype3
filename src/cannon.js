function Cannon(game, x, y, type, sprite){
    var cannon_info = game.game.cannon_config["cannons"][type];
    this.x = x;
    this.y = y;
    this.type = type;
    this.sprite = sprite;
    this.cannonName = cannon_info.cannonName;
    this.damage = cannon_info.damage;
    this.range = cannon_info.range;
    this.fireRate = cannon_info.fireRate;
    this.actualRange = this.range * 15;
    this.upgrade = cannon_info.upgrades[0];
    this.upgradeLevel = 0;

    this.target = null;
    this.rechargeTime = 250 / this.fireRate;
    this.timePassed = this.rechargeTime + 5;

    this.rotateTween;

    this.sprite.on('pointerdown', function(){
        //Show radius
        if(game.selectedCannon != null)
            game.selectedCannon.sprite.rangeCircleGraphics.alpha = 0;

        game.selectedCannon = this;
        this.sprite.rangeCircleGraphics.alpha = 0.2;

        //Show the cannon information in the sidebar
        game.sidebar.displayCannonInfo(this);
    }, this);

    this.onHit = function(enemy){
        enemy.hit(this.damage);
    };

    this.update = function(delta){

        //Find the nearest enemy
        if(this.timePassed > this.rechargeTime)
        {
            for(var i = 0; i < game.enemies.getChildren().length; i++)
            {
                if(Phaser.Geom.Intersects.CircleToRectangle(this.sprite.rangeCircle, game.enemies.getChildren()[i].getBounds()))
                {
                    if(game.enemies.getChildren()[i].active)
                    {
                        this.target = game.enemies.getChildren()[i];
                        //TODO: Instead of target.x predict the position of the enemy
                        /*
                        //This returns a number between 0-1
                        var progress = this.target.pathTween.progress;

                        //This requires a number between 0-1
                        var predictedPoint = this.target.path.getPoint();
                        */

                        var angle = Phaser.Math.Angle.Between(this.sprite.x, this.sprite.y, this.target.x, this.target.y);
                        var shortestAngle = Phaser.Math.Angle.ShortestBetween(this.sprite.angle, Phaser.Math.RadToDeg(angle));
                        var newAngle = this.sprite.angle + shortestAngle; 
                        this.rotateTween = game.tweens.add({
                            targets: this.sprite,
                            angle: newAngle,
                            ease: 'Linear',
                            duration: 500,
                            paused: false,
                        });
                        break;
                    }
                }

                this.target = null;
            }
        }

        if(this.target != null){

            var angle = Phaser.Math.Angle.Between(this.sprite.x, this.sprite.y, this.target.x, this.target.y);
            var shortestAngle = Phaser.Math.Angle.ShortestBetween(this.sprite.angle, Phaser.Math.RadToDeg(angle));
            var newAngle = this.sprite.angle + shortestAngle; 
            //this.rotateTween.updateTo('angle', newAngle);

            if(!this.target.active)
                this.target = null;

            if(this.timePassed > this.rechargeTime){
                game.bulletPool.getBullet(this.x, this.y, this.onHit.bind(this), this.target);
            }

            if(this.timePassed > this.rechargeTime)
                this.timePassed = 0;
        }

        

        this.timePassed += delta / 15;

    }

    this.upgradeCannon = function(){
        this.damage += this.upgrade.damage;
        this.range += this.upgrade.range;
        this.fireRate += this.upgrade.fireRate;

        //redraw range circle
        this.sprite.rangeCircle.radius = this.range * 15;
        this.sprite.rangeCircleGraphics.clear();
        this.sprite.rangeCircleGraphics.fillCircleShape(this.sprite.rangeCircle);

        this.upgradeLevel++;
        this.upgrade = game.game.cannon_config["cannons"][type].upgrades[this.upgradeLevel];

        game.sidebar.displayCannonInfo(this);

    };

    this.destroyCannon = function(){
        game.cannons.splice(game.cannons.indexOf(this), 1);
        game.obstacles.splice(game.obstacles.indexOf(this.sprite.getBounds()), 1);
        this.target = null;
        this.sprite.rangeCircle = null;
        this.sprite.rangeCircleGraphics.destroy();
        this.sprite.destroy();
        game.sidebar.hideCannonInfo();
    };
}

function PlaceCannon(game, go, x, y){
    for(var i = 0; i < game.obstacles.length; i++){
        if(Phaser.Geom.Rectangle.Overlaps(go.getBounds(), go.scene.obstacles[i])){
            go.destroy();
            return;
        }
    }

    game.cash -= game.game.cannon_config["cannons"][go.cannonType].cost;
    game.updateCashText();
    game.input.setDraggable(go, false);

    var special = game.game.cannon_config["cannons"][go.cannonType].special;
    var cannon;
    switch (special) {
        case "None":
            cannon = new Cannon(game, x, y, go.cannonType, go);
            break;
        case "Slow":
            cannon = new SlowCannon(game, x, y, go.cannonType, go);
            break;
        default:
            cannon = new Cannon(game, x, y, go.cannonType, go);
            break;
    }
    
    
    game.cannons.push(cannon);
    game.obstacles.push(go.getBounds());
}

Cannon.prototype = Object.create(Cannon);
Cannon.prototype.constructor = Cannon;
function BulletPool(game){

    this.bullets = game.add.group();
    this.bullets.createMultiple({repeat: 30, key: 'projectile', active: false, visible: false} );

    this.getBullet = function(x, y, onHitCallback, target){
        if(!target)
            return;

        var bullet = this.bullets.get(x, y, 'projectile');
        bullet.setActive(true);
        bullet.setVisible(true);
        bullet.setDepth(50);
        bullet.onHitCallback = onHitCallback;
        bullet.target = target;

        bullet.tween = game.tweens.add({
            targets: bullet,
            x: target.x,
            y: target.y,
            ease: 'Linear.None',
            duration: 750,
            onComplete: function(){
                bullet.setActive(false);
                bullet.setVisible(false);
            },
            paused: false
        });
        
        return bullet;
    }

    this.update = function(){
        for(var i = 0; i < this.bullets.getChildren().length; i++){
            if(this.bullets.getChildren()[i].active){
                var bullet = this.bullets.getChildren()[i];
                bullet.tween.updateTo('x', bullet.target.x);
                bullet.tween.updateTo('y', bullet.target.y);

                for(var j = 0; j < game.enemies.getChildren().length; j++){
                    if(Phaser.Geom.Rectangle.Overlaps(bullet.getBounds(), game.enemies.getChildren()[j].getBounds())){
                        bullet.onHitCallback(game.enemies.getChildren()[j]);
                        bullet.tween.stop();
                        bullet.setActive(false);
                        bullet.setVisible(false);
                        break;
                    }
                }
            }
        }
    }
}

BulletPool.prototype = Object.create(BulletPool);
BulletPool.prototype.constructor = BulletPool;


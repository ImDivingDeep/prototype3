var cannon_config = {
    0 : {
        cannonName : 'Archer',
        damage : 5,
        range: 10,
        fireRate: 5,
        cost: 100,
        sprite: 'cannon1',
        upgrades: {
            0 : {
                damage: 1,
                range: 2,
                fireRate: 0,
                cost: 50
            },
            1 : {
                damage: 2,
                range: 1,
                fireRate: 1,
                cost: 75
            }
        }
    },
    1 : {
        cannonName: 'Cannon2',
        damage : 6,
        range: 12,
        fireRate: 5,
        cost: 150,
        sprite: 'cannon2',
        upgrades: {
            0 : {
                damage: 1,
                range: 2,
                fireRate: 1,
                cost: 50
            },
            1 : {
                damage: 2,
                range: 1,
                fireRate: 1,
                cost: 75
            }
        }
    }
}
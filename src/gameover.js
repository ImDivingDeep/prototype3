var GameOver = Phaser.Class({
    Extends: Phaser.Scene,

    initialize:

    function GameOver ()
    {
        Phaser.Scene.call(this, { key: 'GameOver', active: false });
    },

    init: function(data){
        this.currentLevel = data.level;
    },

    preload: function ()
    {
    },

    create: function ()
    {
        this.gameOverDialog = new Dialog(this, 960, 540, 'dialog');
        this.gameOverDialog.addText(0, -200, 'Game over!', 40);
        this.gameOverDialog.addButton(-100, 100, 'retry', 'button', 'Retry', 35).on('pointerdown', function(){
            this.scene.start('GameScreen', {level: this.currentLevel});
        }, this);
        this.gameOverDialog.addButton(100, 100, 'return', 'button', 'Menu', 35).on('pointerdown', function(){
            this.scene.stop('GameScreen');
            this.scene.start('LevelSelect');
        }, this);
        this.gameOverDialog.open();
        console.log(this);
    },
});
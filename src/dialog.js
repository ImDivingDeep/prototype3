function Dialog(game, x, y, background){
    this.background = game.add.sprite(x, y, background);

    //Put this dialog in a container so every element is shown in front of the game
    this.uiContainer = game.add.container();
    this.uiContainer.add(this.background);
    this.uiContainer.visible = false;
    this.uiContainer.setDepth(100);

    this.buttons = [];

    this.addButton = function(x, y, name, image, text, textSize)
    {
        var sprite = game.add.sprite(this.background.x + x, this.background.y + y, image).setInteractive();
        this.uiContainer.add(sprite);
        if(text)
        {
            if(!textSize) textSize = 25;
            var textobject = game.add.text(this.background.x + x, this.background.y + y, text, { fontSize: textSize, fill: '#000'});
            textobject.setOrigin(0.5);
            
            this.uiContainer.add(textobject);
        }
        this.buttons.push({'name': name, 'sprite': sprite});
        
        return sprite;
    }

    this.addText = function(x, y, text, textSize)
    {
        if(!textSize) textSize = 25;
        var textobject = game.add.text(this.background.x + x, this.background.y + y, text, { fontSize: textSize, fill: '#000'});
        textobject.setOrigin(0.5);
        this.uiContainer.add(textobject);
        return textobject;
    }

    this.addImage = function(x, y, imageName)
    {
        var sprite = game.add.sprite(this.background.x + x, this.background.y + y, imageName);
        this.uiContainer.add(sprite);
        return sprite;
    }

    this.open = function()
    {
        this.uiContainer.visible = true;
    }

    this.close = function()
    {
        this.uiContainer.visible = false;
    }
}

Dialog.prototype = Object.create(Dialog);
Dialog.prototype.constructor = Dialog;
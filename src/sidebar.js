function Sidebar(game, level) {
    this.game = game;
    this.background = new Phaser.Geom.Rectangle(1440, 0, 480, game.scale.height);
    this.cannonSprites = [];
    this.selectedCannon = null;
    var graphics = game.add.graphics({ fillStyle: { color: 0x7371fc } });
    graphics.fillRectShape(this.background);

    this.createCannonSprite = function(game, x, y, type)
    {
        var sprite = game.add.sprite(x, y, game.game.cannon_config["cannons"][type].sprite);
        sprite.setInteractive();
        sprite.setScale(1.8);
        sprite.cannonType = type;
        game.input.setDraggable(sprite);
        sprite.on('dragstart', OnDragStart);
        sprite.on('drag', OnDrag);
        sprite.on('dragend', OnDragEnd);
        sprite.origX = sprite.x;
        sprite.origY = sprite.y;
        this.cannonSprites.push(sprite);
        return sprite;
    }

    //Generate the cannons in the sidebar
    var count = 0;
    for(var i = 0; i < 3; i++){
        for(var j = 0; j < 3; j++){
            game.add.sprite(this.background.x + (120 * j) + 90, (150 * i) + 100 , 'button').setScale(1.8);
            
            if(levels[level].cannons[count] !== undefined){
                this.createCannonSprite(game, this.background.x + (120 * j) + 90, (150 * i) + 100, levels[level].cannons[count]);
                game.add.text(this.background.x + (120 * j) + 90, (150 * i) + 175, game.game.cannon_config["cannons"][levels[level].cannons[count]].cost, {fontSize: 30, fill: '#FFFFFF'}).setOrigin(0.5);
            }

            count++;
        }
    }

    //Wave indicator
    this.waveText = game.add.text(this.background.x + 200, 900,'Wave ', { fontSize: 30, fill: '#FFFFFF'}).setOrigin(0.5);

    this.setWaveText = function(currentWave){
        this.waveText.text = "Wave " + currentWave + "/" + levels[level].waves.length;
    }

    //Start wave button
    var startWaveButton = game.add.sprite(this.background.x + 200, 1040, 'button').setInteractive();
    var startWaveButtonText = game.add.text(this.background.x + 200, 1040,'Start Wave', { fontSize: 25, fill: '#FFFFFF'}).setOrigin(0.5);
    startWaveButton.setScale(4, 1);

    startWaveButton.on('pointerdown', function(){
        game.startWave();
        startWaveButton.destroy();
        startWaveButtonText.destroy();
    }, this);

    //cannon info dialog

    this.selectedCannonGroup = game.add.container();
    this.selectedCannonNameText = game.add.text(this.background.x + 200, 490,'Test', { fontSize: 40, fill: '#FFFFFF'}).setOrigin(0.5);
    this.selectedCannonRangeText = game.add.text(this.background.x + 200, 550,'Range: 30 (+5)', { fontSize: 30, fill: '#FFFFFF'}).setOrigin(0.5);
    this.selectedCannonFirerateText = game.add.text(this.background.x + 200, 600,'Firerate: 30 (+5)', { fontSize: 30, fill: '#FFFFFF'}).setOrigin(0.5);
    this.selectedCannonDamageText = game.add.text(this.background.x + 200, 650,'Damage: 10 (+0)', { fontSize: 30, fill: '#FFFFFF'}).setOrigin(0.5);
    this.selectedCannonUpgradeCostText = game.add.text(this.background.x + 300, 750, '',  { fontSize: 30, fill: '#FFFFFF'}).setOrigin(0.5);
    this.selectedCannonUpgradeButton = game.add.sprite(this.background.x + 200, 750, 'button').setInteractive();
    this.selectedCannonDestroyButton = game.add.sprite(this.background.x + 200, 825, 'button').setInteractive();

    this.selectedCannonGroup.add([this.selectedCannonNameText, this.selectedCannonRangeText, this.selectedCannonFirerateText,this.selectedCannonDamageText,
        this.selectedCannonUpgradeCostText, this.selectedCannonUpgradeButton, this.selectedCannonDestroyButton]);

    this.selectedCannonGroup.setVisible(false);

    this.displayCannonInfo = function(cannon)
    {
        this.selectedCannonGroup.setVisible(true);
        this.selectedCannon = cannon;
        this.selectedCannonNameText.text = cannon.cannonName;

        if(this.selectedCannon.upgrade != null){
            this.selectedCannonRangeText.text = 'Range: ' + cannon.range + '(+' + cannon.upgrade.range + ')';
            this.selectedCannonFirerateText.text = 'Fire rate: ' + cannon.fireRate + '(+' + cannon.upgrade.fireRate + ')';
            this.selectedCannonDamageText.text = 'Damage: ' + cannon.damage + '(+' + cannon.upgrade.damage + ')';
            this.selectedCannonUpgradeCostText.text = cannon.upgrade.cost;
            this.selectedCannonUpgradeButton.visible = true;
        }
        else //If there are no further upgrades
        {
            this.selectedCannonRangeText.text = 'Range: ' + cannon.range;
            this.selectedCannonFirerateText.text = 'Fire rate: ' + cannon.fireRate;
            this.selectedCannonDamageText.text = 'Damage: ' + cannon.damage;
            this.selectedCannonUpgradeCostText.text = '';
            this.selectedCannonUpgradeButton.visible = false;
        }
    }

    this.hideCannonInfo = function(){
        this.selectedCannonGroup.setVisible(false);
        this.selectedCannon = null;
    }

    this.selectedCannonUpgradeButton.on('pointerdown', function(){
        if(this.game.cash >= this.selectedCannon.upgrade.cost){
            this.game.cash -= this.selectedCannon.upgrade.cost;
            this.game.updateCashText();
            this.selectedCannon.upgradeCannon();
        }
    }, this);

    this.selectedCannonDestroyButton.on('pointerdown', function(){
        this.selectedCannon.destroyCannon();
    }, this);

    //Update the sidebar icons to display if they can be bought
    this.updateSidebarIcons = function(){
        for(var i = 0; i < this.cannonSprites.length; i++){
            if(game.cash >= game.game.cannon_config["cannons"][this.cannonSprites[i].cannonType].cost){
                game.input.setDraggable(this.cannonSprites[i]);
                this.cannonSprites[i].alpha = 1;
            }
            else{
                game.input.setDraggable(this.cannonSprites[i], false);
                this.cannonSprites[i].alpha = 0.5;
            }
        }
    }
}



function OnDragStart(pointer, dragX, dragY){
    this.rangeCircle = new Phaser.Geom.Circle(this.x, this.y, this.scene.game.cannon_config["cannons"][this.cannonType].range * 15);
    this.rangeCircleGraphics = this.scene.add.graphics({ fillStyle: { color: 0xff0000 }});
    this.rangeCircleGraphics.alpha = 0.2;
    this.rangeCircleGraphics.fillCircleShape(this.rangeCircle);
}

function OnDrag(pointer, dragX, dragY){
    this.x = dragX;
    this.y = dragY;
    this.rangeCircleGraphics.clear();
    this.rangeCircle.x = dragX;
    this.rangeCircle.y = dragY;
    this.rangeCircleGraphics.fillCircleShape(this.rangeCircle);
}

function OnDragEnd(pointer)
{
    this.rangeCircleGraphics.alpha = 0;
    this.scene.sidebar.cannonSprites.splice(this.scene.sidebar.cannonSprites.indexOf(this), 1);
    //Create a new sprite in the sidebar
    this.scene.sidebar.createCannonSprite(this.scene, this.origX, this.origY, this.cannonType);

    PlaceCannon(this.scene, this, this.x, this.y);
    
}

Sidebar.prototype = Object.create(Sidebar);
Sidebar.prototype.constructor = Sidebar;
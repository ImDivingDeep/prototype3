var enemy_config = {
    0: {
        speed: 5,
        health: 10,
        reward: 15,
    },
    1: {
        speed: 5,
        health: 25,
        reward: 25,
    },
    2: {
        speed: 8,
        health: 35,
        reward: 30,
    },
    3: {
        speed: 12,
        health: 10,
        reward: 30,
    },
    4: {
        speed: 6,
        health: 150,
        reward: 30,
    },
}
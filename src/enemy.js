function Enemy(game, delay, type) {
    //Inherit from pathfollower
    Phaser.GameObjects.PathFollower.call(this, game, game.path, 0, 0, 'enemy1');
    game.add.existing(this);
    this.config = enemy_config[type];
    this.type = type;
    this.speed = this.config.speed;
    this.health = this.config.health;
    this.reward = this.config.reward;
    this.resetSlowEvent = null;

    this.hit = function(damage){
        this.health -= damage;
        if(this.health <= 0){
            game.cash += this.reward;
            Animations.collectMoney(game, this.x, this.y, this.reward);
            game.updateCashText();
            game.enemies.remove(this, true, true);
            //Stop the enemy's animation seperately
            this.pathTween.stop();
        }
        else{
            Animations.enemyHit(this);
        }
    };

    this.completePath = function(_tween, _targets, enemy){
        game.health.decrease(5);
        game.enemies.remove(enemy, true, true);
    };

    this.startFollow({
        'duration': (game.path.getLength() / this.speed) * 50, 
        'positionOnPath': true, 
        'rotateToPath': true,
        'delay': delay, 
        'onComplete': this.completePath,
        'onCompleteParams': this
    });

    this.slowDown = function(){
        //TODO: Add these values (amount, duration) to parameters
        this.pathTween.setTimeScale(0.5);
        this.resetSlowEvent = game.time.addEvent({
            delay: 3000,
            callback: function(){
                this.pathTween.setTimeScale(1);
            },
            callbackScope: this,
        });
    }

}

Enemy.prototype = Object.create(Phaser.GameObjects.PathFollower.prototype);
Enemy.prototype.constructor = Enemy;
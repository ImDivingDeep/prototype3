var Animations = {

    game: null,
    textGroup: null,
    spriteGroup: null,

    init: function(game){
        this.game = game;
        textGroup = this.game.add.group({classType: Phaser.GameObjects.Text, active: false});
    },

    collectMoney: function(game, x, y, amount){

        var text = textGroup.get(x, y, "$"+ amount, { fontSize: 40, fill: '#00FF00'});
        text.text = "$"+ amount;
        text.setStroke('#000000', 6);
        game.tweens.add({
            targets: text,
            y: y - 50,
            ease: 'Linear',
            duration: 1500,
            paused: false,
            onStart: function(tween){
                text.active = true;
                text.visible = true;
            },
            onComplete: function(tween){
                text.active = false;
                text.visible = false;
            },
        });

    },

    enemyHit: function(sprite){
        this.game.tweens.add({
            targets: sprite,
            duration: 100,
            scaleX: '1.2',
            scaleY: '1.2',
            ease: 'Sine.easeInOut',
            yoyo: true,
            onComplete: function(tween, targets){
                targets[0].scaleX = 1;
                targets[0].scaleY = 1;
            },
        });
    },

}
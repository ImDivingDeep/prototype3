var LevelSelect = Phaser.Class({
    Extends: Phaser.Scene,

    initialize:

    function LevelSelect ()
    {
        Phaser.Scene.call(this, { key: 'LevelSelect', active: false });
    },

    preload: function ()
    {
        Animations.init(this);
    },

    create: function ()
    {
        var count = 0;
        for(level in levels){
            var button = this.add.sprite((300 * count) + 200, 200, 'button').setScale(3); 
            this.add.text((300 * count) + 200, 200, count, {fontSize: 50, fill: '#FFFFFF'}).setOrigin(0.5);
            button.levelToLoad = count;
            button.setInteractive();
            button.on('pointerdown', function(){
                this.scene.scene.start('GameScreen', {level: this.levelToLoad});
            });
            count++;
        }
    },
});
function SlowCannon(game, x, y, type, sprite){
    Cannon.call(this, game, x, y, type, sprite);

    this.onHit = function(enemy){
        enemy.hit(this.damage);
        enemy.slowDown();
    };
};

SlowCannon.prototype = Object.create(Cannon);
SlowCannon.prototype.constructor = SlowCannon;


var LevelWon = Phaser.Class({
    Extends: Phaser.Scene,

    initialize:

    function LevelWon ()
    {
        Phaser.Scene.call(this, { key: 'LevelWon', active: false });
    },

    preload: function ()
    {
    },

    init: function(data)
    {
        this.currentLevel = data.level;
    },

    create: function ()
    {
        this.levelWonDialog = new Dialog(this, 960, 540, 'dialog');
        this.levelWonDialog.addText(0, -200, 'Game won!', 40);
        this.levelWonDialog.addButton(-100, 100, 'next', 'button', 'Next', 35).on('pointerdown', function(){
            //start the next level
            this.scene.start('GameScreen', {level: ++this.currentLevel});
            this.scene.stop();
        }, this);
        this.levelWonDialog.addButton(100, 100, 'return', 'button', 'Menu', 35).on('pointerdown', function(){
            this.scene.stop('GameScreen');
            this.scene.start('LevelSelect');
        }, this);
        this.levelWonDialog.open();
    },
});
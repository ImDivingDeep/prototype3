function Healthbar(game, x, y, maxHealth) {
    this.bar = new Phaser.GameObjects.Graphics(game);
    this.x = x;
    this.y = y;
    this.width = 150;
    this.height = 25;
    this.percentage = (this.width - 4) / maxHealth;
    this.value = maxHealth;

    this.draw = function(){

        this.bar.clear();

        //Black border
        this.bar.fillStyle(0x000000);
        this.bar.fillRect(this.x, this.y, this.width, this.height);

        //White background
        this.bar.fillStyle(0xffffff);
        this.bar.fillRect(this.x + 2, this.y + 2, this.width - 4, this.height - 4);

        //Color the bar red if its below a certain value
        //TODO: Change this value to a calculated percentage
        if (this.value < 30)
        {
            this.bar.fillStyle(0xff0000);
        }
        else
        {
            this.bar.fillStyle(0x00ff00);
        }

        //Calculate the width of the new bar
        var healthWidth = Math.floor(this.percentage * this.value);

        this.bar.fillRect(this.x + 2, this.y + 2, healthWidth, this.height - 4);

    };

    this.decrease = function(amount){

        this.value -= amount;

        if (this.value < 0)
        {
            this.value = 0;
        }

        this.draw();

    };

    game.add.existing(this.bar);

    this.draw();
}

Healthbar.prototype = Object.create(Healthbar);
Healthbar.prototype.constructor = Healthbar;
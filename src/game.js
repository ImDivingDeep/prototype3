var GameScreen = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize: function GameScreen ()
    {
        Phaser.Scene.call(this, { key: 'GameScreen', active: false });
        
    },

    init: function (data)
    {
        this.currentLevel = data.level;
    },

    preload: function ()
    {
        Animations.init(this);
    },

    create: function ()
    {
        this.currentWave = 0;
        this.enemies = this.add.group();
        this.cannons = [];
        this.selectedCannon = null;
        this.gameRunning = false;
        this.obstacles = [];
        this.cash = levels[this.currentLevel].startingCash;
        this.score = 0;

        this.bulletPool = new BulletPool(this);

        //Sidebar
        this.sidebar = new Sidebar(this, this.currentLevel);
        this.sidebar.setWaveText(this.currentWave);
        this.obstacles.push(this.sidebar.background);

        //UI
        this.cashText = this.add.text(25, 25, this.cash, { fontSize: 30, fill: '#000000'});
        this.scoreText = this.add.text(25, 1035, 'Score: 0', { fontSize: 30, fill: '#000000'});
        this.pauseMenuButton = this.add.image(1350, 35, 'button').setInteractive();
        this.pauseMenuButton.on('pointerdown', function(){
            this.scene.launch('PauseMenu');
            this.scene.pause();
        }, this);
        
        var points = levels[this.currentLevel].points;

        this.path = new Phaser.Curves.Path(points[0].x, points[0].y);

        for(var i = 1; i < points.length; i++){
            this.path.lineTo(points[i].x, points[i].y);
            var bounds = this.path.curves[i-1].getBounds();
            this.obstacles.push(bounds);
            
            var difX = Math.abs(this.path.curves[i-1].p0.x - this.path.curves[i-1].p1.x) + 32;
            var difY = Math.abs(this.path.curves[i-1].p0.y - this.path.curves[i-1].p1.y) + 32;
            var posX = Math.abs(points[i].x + points[i-1].x) / 2;
            var posY = Math.abs(points[i].y + points[i-1].y) / 2;
            //Draw texture between points
            this.add.tileSprite(posX, posY, difX, difY, 'stone').setOrigin(0.5);
        }

        this.health = new Healthbar(this, points[points.length - 1].x, points[points.length - 1].y, 100);

        this.input.on('pointerdown', function(_pointer, go){
            if(go.length == 0 && this.selectedCannon != null){
                this.selectedCannon.sprite.rangeCircleGraphics.alpha = 0;
                this.selectedCannon = null;
                this.sidebar.hideCannonInfo();
            }
        }, this);

    },

    update: function(time, delta){
        this.bulletPool.update();

        for(var i = 0; i < this.cannons.length; i++){
            this.cannons[i].update(delta);
        }

        if(this.gameRunning && this.enemies.getLength() == 0){
            this.gameRunning = false;
            this.currentWave++;
            this.startWave();
        }

        if(this.health.value === 0){
            //gameover
            this.scene.launch('GameOver', {level: this.currentLevel});
            this.scene.pause();
        }
    },

    startWave: function()
    {
        if(!levels[this.currentLevel].waves[this.currentWave]){
            console.log('level finished');
            this.scene.launch('LevelWon', {level: this.currentLevel});
            this.scene.pause();
            return;
        }

        var enemyCount = 0;
        for(var i = 0; i < levels[this.currentLevel].waves[this.currentWave].enemies.length; i++){
            for(var j = 0; j < levels[this.currentLevel].waves[this.currentWave].enemies[i].amount; j++){
                this.enemies.add(new Enemy(this, enemyCount * 1000, levels[this.currentLevel].waves[this.currentWave].enemies[i].type));
                enemyCount++;
            }
        }
        

        this.sidebar.setWaveText(this.currentWave + 1, levels[this.currentLevel].waves.length);
        this.gameRunning = true;
    },

    updateCashText: function()
    {
        this.cashText.text = this.cash;
        this.sidebar.updateSidebarIcons();
    }

});
var PauseMenu = Phaser.Class({
    Extends: Phaser.Scene,

    initialize:

    function PauseMenu ()
    {
        Phaser.Scene.call(this, { key: 'PauseMenu', active: false });
    },

    preload: function ()
    {
    },

    create: function ()
    {
        this.pauseMenuDialog = new Dialog(this, 960, 540, 'dialog');
        this.pauseMenuDialog.addText(0, -200, 'Game paused!', 40);
        this.pauseMenuDialog.addButton(-100, 100, 'retry', 'button', 'Resume', 35).on('pointerdown', function(){
            this.scene.resume('GameScreen');
            this.scene.stop();
        }, this);
        this.pauseMenuDialog.addButton(100, 100, 'return', 'button', 'Menu', 35).on('pointerdown', function(){
            this.scene.stop('GameScreen');
            this.scene.start('LevelSelect');
        }, this);
        this.pauseMenuDialog.open();
    },
});